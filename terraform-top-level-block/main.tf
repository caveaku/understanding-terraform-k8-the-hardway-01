
# Create a VPC
resource "aws_vpc" "seantech_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "seantech_vpc"
  }
}

##RESOURCE_NAME.LOCAL_NAME.DESIRED_ATTRIBUTE

resource "aws_subnet" "subnet_1" {
  vpc_id            = local.vpc_id
  cidr_block        = var.subnet_1_cidr
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "subnet_1"
  }
}
resource "aws_subnet" "subnet_2" {
  vpc_id            = local.vpc_id
  cidr_block        = var.subnet_2_cidr
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "subnet_2"
  }
}

resource "aws_instance" "web" {
  ami           = "ami-0cc87e5027adcdca8" ## use datasource to pull down ec2 ami
  instance_type = "t2.micro"

  tags = {
    Name = "web"
  }
}

