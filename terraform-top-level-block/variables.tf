
variable "subnet_1_cidr" {
  description = "Value for subnet1 cidr"
  type        = string
  default     = "10.0.1.0/24"
}

# variable "subnet_1_az" {
#   description = "Value for subnet1 az"
#   type = string
#   default = "us-east-1c"
# }

variable "subnet_2_cidr" {
  description = "Value for subnet2 cidr"
  type        = string
  default     = "10.0.2.0/24"
}

# variable "subnet_2_az" {
#   description = "Value for subnet2 az"
#   type = string
#   default = "us-east-1b"
# }


